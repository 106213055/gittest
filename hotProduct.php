<?php
session_start();
require("orderModel.php");

//check whether the user has logged in or not
if ( ! isSet($_SESSION["loginProfile"] )) {
	//if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Basic HTML Examples</title>
</head>
<body>
<p>This is the Product Management page 
[<a href="logout.php">logout</a>] [<a href="admin.php">Admin Main Page</a>]

</p>
<hr>
<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
	", Your ID is: ", $_SESSION["loginProfile"]["uID"],
	", Your Role is: ", $_SESSION["loginProfile"]["uRole"],"<HR>";
	$result=getOrderItem($ordID,$prdID,$quantity);
?>
<hr>
	<table width="200" border="1">
  <tr>
    <td>id</td>
    <td>數量</td>
  </tr>
<?php
while (	$rs=mysqli_fetch_assoc($result)) {
	echo "<td>" , $rs['prdID'], "</td>";
    echo "<td>" , $rs['sum(quantity)'], "</td>";
}
?>
</table>

</body>
</html>

<?php
require_once("dbconfig.php");

function getOrderList($uID) {
	global $db;
	$sql = "SELECT ordID, orderDate, status FROM userOrder WHERE uID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getConfirmedOrderList() {
	global $db;
	$sql = "SELECT ordID, uID, orderDate,address FROM userOrder WHERE status=1 order by address";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	//mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}
//抓訂單的id
function _getCartID($uID) {
	//get an unfished order (status=0) from userOrder
	global $db;
	$sql = "SELECT ordID FROM userorder WHERE uID=? and status=0";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	if ($row=mysqli_fetch_assoc($result)) {
		return $row["ordID"];
	} else {
		//no order with status=0 is found, which means we need to creat an empty order as the new shopping cart
		$sql = "insert into userOrder ( uID, status ) values (?,0)";
		$stmt = mysqli_prepare($db, $sql); //prepare sql statement
		mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
		mysqli_stmt_execute($stmt);  //執行SQL
		$newOrderID=mysqli_insert_id($db);
		return $newOrderID;
	}
}
//查看現有商品數目
function findprd($ordID,$prdID){
    global $db;
    //如果訂單相同且商品種類一樣
    //ss:轉換並固定$ordID, $prdID的string型態
	$sql ="SELECT ordID, prdID, quantity FROM orderItem WHERE ordID=? and prdID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ss", $ordID, $prdID); //bind parameters with variables
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt); //get the results
	if ($row=mysqli_fetch_assoc($result)){
        return $row["quantity"];
    } else {
        return 0;
    }
}
//ii(轉換並固定$ordID, $prdID的int型態)
function addToCart($uID, $prdID) {
	global $db;
	$ordID= _getCartID($uID);
    //商品原有數目值
    $origin = findprd($ordID, $prdID);
    if ($origin==0){
        $sql = "insert into orderItem (ordID, prdID, quantity) values (?,?,1);";
        $stmt = mysqli_prepare($db, $sql); //prepare sql statement
	    mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
        mysqli_stmt_execute($stmt);  //執行SQL
    }else{
        //如果商品原本就有，數量+1
        $have = $origin+1;//have:回傳產品單項總數量
        $sql = "UPDATE orderItem SET quantity=? where ordID=? and prdID=?";
        $stmt = mysqli_prepare($db, $sql); //prepare sql statement
        //iii(轉換並固定$have,$ordID, $prdID的int型態)
	    mysqli_stmt_bind_param($stmt, "iii", $have, $ordID, $prdID); //bind parameters with variables
        mysqli_stmt_execute($stmt);  //執行SQL
    }
}

function removeFromCart($serno) {
	global $db;
	$sql = "delete from orderItem where serno=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $serno); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function checkout($uID, $address) {
	global $db;
	$ordID= _getCartID($uID);
	$sql = "update userorder set orderDate=now(),address=?,status=1 where ordID=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "si", $address, $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function shipout($ordID) {
	global $db;
	$sql = "update userorder set status=2 where ordID=?;";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	return mysqli_stmt_execute($stmt);  //執行SQL
}

function getCartDetail($uID) {
	global $db;
	$ordID= _getCartID($uID);
	$sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}


function getOrderDetail($ordID) {
	global $db;
	$sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}

function getOrderItem($ordID,$prdID,$quantity) {
	global $db;
	$sql = "SELECT prdID,sum(quantity) FROM orderitem WHERE 1 group by prdID order by sum(quantity) DESC";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
	return $result;
}
?>










